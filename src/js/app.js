import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();

// Бургер меню
let menuBtn = document.querySelector('.icon-menu');
let menu = document.querySelector('.header__top-menu');
menuBtn.addEventListener('click', function(){
	menu.classList.toggle('active');
	menuBtn.classList.toggle('active');  
})
